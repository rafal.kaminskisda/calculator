import org.junit.jupiter.api.*;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class CalculatorTest {
    // @BeforeAll, @BeforeEach, @AfterAll, AfterEach

    Calculator calculator;


    @Test
    @DisplayName("Assertion which return sum - positive")
    void shouldAddTwoNumber() {
        //given
        int number1 = 2;
        int number2 = 5;

        //when
        int sum = calculator.add(number1, number2);

        //then
        assertEquals(7, sum);
    }

    @Test
    @DisplayName("Assertion which return subtract - positive")
    void shouldSubtractTwoNumber() {
        //given
        int number1 = 2;
        int number2 = 5;

        //when
        int sum = calculator.subtract(number1, number2);

        //then
        assertEquals(-3, sum);
    }

    @Test
    @DisplayName("Assertion which return multiply - positive")
    void shouldMultiplyTwoNumber() {
        //given
        int number1 = 2;
        int number2 = 5;

        //when
        int sum = calculator.multiply(number1, number2);

        //then
        assertEquals(10, sum);
    }

    @BeforeEach
    void beforeEach() {
        calculator = new Calculator();
    }


}
